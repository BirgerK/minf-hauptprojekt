import time
import traceback
import json
import xmltodict
from pathlib import Path
from multiprocessing import Process
from xml.parsers.expat import ExpatError

import numpy as np
import pymysql
from pymongo import MongoClient
from urllib3 import PoolManager, util as lib3util
from urllib3.util import Retry
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

sql_password = os.environ.get("SQL_PASSWORD")
wpt_basic_auth = os.environ.get("WPT_BASIC_AUTH")

osm_instance_id = 1
ignore_fields = ['indexWithinHar']

wpt_cache_path = '/tmp/wpt/'

number_of_processes = 10
processes = []


def extract_relevant_information_from_xml(process_number, xml):
    xml_data = xml['response']['data']
    result = {}
    if 'webPagetestVersion' in xml['response']:
        result['steps'] = xml_data['run']['firstView']['step']
        result['location'] = xml_data['location']
        result['bandwidth_down'] = xml_data['bwDown']
        result['bandwidth_up'] = xml_data['bwUp']
        result['packet_loss_rate'] = xml_data['plr']
        result['latency'] = xml_data['latency']
        if not isinstance(result['steps'], list):
            print('process ' + str(process_number) +
                  ' seems like the step is not a list. I will make it a list!')
            result['steps'] = [result['steps']]
        for step in result['steps']:
            try:
                if step['requests'] != None:
                    step['requests'] = step['requests']['request']
                else:
                    step['requests'] = []
                if not isinstance(step['requests'], list):
                    step['requests'] = [step['requests']]
            except TypeError:
                print('process ' + str(process_number) + ' Exception: {}\n was thrown in step: {}'.format(
                    traceback.format_exc(), json.dumps(step)))
    else:
        result['steps'] = []
        result['location'] = xml_data['location']
        result['bandwidth_down'] = xml_data['bwDown']
        result['bandwidth_up'] = xml_data['bwUp']
        result['packet_loss_rate'] = xml_data['plr']
        result['latency'] = xml_data['latency']

        wpt_requests = xml_data['run']['firstView']['requests']['request']
        wpt_steps = xml_data['run']['firstView']['results']['testStep']
        if not isinstance(result['steps'], list):
            result['steps'] = [result['steps']]
        for wpt_step in wpt_steps:
            temp_wpt_step = wpt_step
            wpt_step = {}
            wpt_step['results'] = temp_wpt_step
            wpt_step['eventName'] = temp_wpt_step['eventName']
            number_of_requests_for_step = int(wpt_step['results']['requests'])
            wpt_step['requests'] = wpt_requests[:number_of_requests_for_step]
            wpt_requests = wpt_requests[number_of_requests_for_step:]
            result['steps'].append(wpt_step)
    return result


def process_stuff(process_number, test_ids):
    print('process ' + str(process_number) + ' is starting')
    number_of_test_ids = len(test_ids)
    process_counter = 0

    db = pymysql.connect(host="172.17.0.6",
                         user="root",
                         passwd=sql_password,
                         db="osm_data")
    client = MongoClient('172.17.0.25', 27017)
    osm_details_db = client['OsmDetailAnalysis']
    result_coll = osm_details_db['aggregatedAssets']

    for test_id in test_ids:
        print('process ' + str(process_number) +
              ' is processing test-id ' + test_id)

        response = ''
        wpt_cache_file = wpt_cache_path + test_id
        if not Path(wpt_cache_file).is_file():
            url = 'http://prod.server01.wpt.iteratec.de/xmlResult.php?test={}&requests=1&multistepFormat=1'.format(
                test_id)
            print('{}: Requesting url {}'.format(process_number, url))
            retries = Retry(redirect=3)
            manager = PoolManager(retries=retries)
            headers = lib3util.make_headers(basic_auth=wpt_basic_auth)
            response = manager.request('GET', url, headers=headers)
            print('{}: Received server-response'.format(process_number))
            response = response.data.decode('utf-8')
            with open(wpt_cache_file, "w") as cache_file:
                cache_file.write(response)
        else:
            url = "cached"
            print(
                '{}: Found wpt-result {} in cache, reading it'.format(process_number, test_id))
            with open(wpt_cache_file, 'r') as cache_file:
                response = cache_file.read()
        try:
            xml_document = xmltodict.parse(response)
        except ExpatError:
            print(
                '{}: The HTTP-Response from URL {} contains a XML-Syntax-Error'.format(
                    process_number, url))
            continue
        print('{}: Parsed XML-document'.format(process_number))
        try:
            relevant_xml_data = extract_relevant_information_from_xml(
                process_number, xml_document)
            print('{}: Extracted relevant view-data'.format(process_number))

            request_counter = 0
            for step in relevant_xml_data['steps']:
                if step['results']['docTime'] != '0':
                    for request in step['requests']:
                        request_counter += 1
                        result_document = {
                            'step_id': test_id + '-' + step['eventName'] + '-' + step['id'],
                            'connections': int(step['results']['connections']),
                            'dom_elements': int(step['results']['domElements']),
                            'doc_time': int(step['results']['docTime']),
                            'location': relevant_xml_data['location'],
                            'bandwidth_down': relevant_xml_data['bandwidth_down'],
                            'bandwidth_up': relevant_xml_data['bandwidth_up'],
                            'packet_loss_rate': relevant_xml_data['packet_loss_rate'],
                            'latency': relevant_xml_data['latency'],
                            'start_time': step['results']['date'],
                            'request_id': test_id + '-' + str(request_counter),
                            'request_bytes_in': int(request['bytesIn']),
                            'request_bytes_out': int(request['bytesOut']),
                            'request_load_ms': int(request['load_ms']),
                            'request_ssl_ms': int(request['ssl_ms']),
                            'request_ttfb_ms': int(request['ttfb_ms']),
                            'request_dns_ms': int(request['dns_ms'] if request['dns_ms'] != '-1' else '0'),
                            'request_is_secure':  request['is_secure']
                        }
                        result_coll.insert_one(result_document)

        except TypeError:
            print('{}: {}\n was thrown in XML: {}'.format(process_number, traceback.format_exc(),
                                                          json.dumps(xml_document)))

        process_counter += 1
        print('process ' + str(process_number) + ' finished ' +
              str(process_counter) + ' tests from ' + str(number_of_test_ids) + ' (' + str(
            format(percentage(process_counter, number_of_test_ids), '.2f')) + '%)')
    db.close()
    print('process ' + str(process_number) + ' is finished')


def _get_values_from_sql(sql_connection, test_id, measured_event_id):
    cursor = sql_connection.cursor()
    cursor.execute(
        '''
        SELECT doc_complete_time_in_millisecs FROM
          ((SELECT * FROM job_result WHERE test_id="{}") jr
          LEFT JOIN (SELECT * FROM event_result WHERE measured_event_id="{}" and cached_view="UNCACHED") er ON jr.id=er.job_result_id)
        '''.format(test_id, measured_event_id)
    )

    sql_result = cursor.fetchall()
    try:
        assert len(sql_result) == 1
    except AssertionError:
        print()
    doc_complete = sql_result[0][0]
    cursor.close()
    return doc_complete


def _get_test_ids_from_sql(sql_connection):
    cursor = sql_connection.cursor()
    cursor.execute(
        '''
        SELECT DISTINCT test_id FROM
          ((SELECT * FROM job_result WHERE wpt_server_baseurl LIKE '%prod.server01.wpt.iteratec.de/') jr
          INNER JOIN (SELECT * FROM event_result WHERE cached_view="UNCACHED" AND doc_complete_time_in_millisecs != 0) er ON jr.id=er.job_result_id)
        '''.format()
    )

    result = []
    for obj in cursor.fetchall():
        result.append(obj[0])
    cursor.close()
    return result


def list_to_chunks(l, n):
    """Split list l in n chunks"""
    split_size = int(len(l) / n)
    result = []
    for i in range(0, len(l), split_size):
        result.append(l[i:i + split_size])
    return result


def is_number(obj):
    try:
        val = int(obj)
        return True
    except ValueError:
        return False


def percentage(part, whole):
    return 100 * float(part) / float(whole)


if __name__ == '__main__':

    if not Path(wpt_cache_path).is_dir():
        Path(wpt_cache_path).mkdir()

    client = MongoClient('172.17.0.25', 27017)

    osm_details_db = client['OsmDetailAnalysis']
    osm_asset_group_coll = osm_details_db['assetRequestGroup']

    # the sql-instance knows only results from prod-osm
    db = pymysql.connect(host="172.17.0.6",
                         user="root",
                         passwd=sql_password,
                         db="osm_data")
    distinct_test_ids = _get_test_ids_from_sql(db)

    print('Found ' + str(len(distinct_test_ids)) + ' different test-ids')

    test_id_chunks = list_to_chunks(
        list(distinct_test_ids), number_of_processes)

    for number_of_process in range(0, number_of_processes):
        p = Process(target=process_stuff, args=(number_of_process,
                                                test_id_chunks[number_of_process]))
        p.start()
        processes.append(p)

    for process in processes:
        process.join()
