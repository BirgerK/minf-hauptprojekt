import { CUSTOM_ELEMENTS_SCHEMA, NgModule, Sanitizer } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertComponent, JhiAlertErrorComponent } from './';
import { CommonModule, DatePipe } from '@angular/common';
import { AlertService, DateUtils, EventManager, NgJhipsterModule } from 'ng-jhipster';
import { SpinnerWrapperComponent } from './spinner-wrapper/spinner-wrapper.component';
import { SpinnerModule } from 'angular2-spinner/dist';
import { SortPipe } from './sortPipe/sort.pipe';

export function alertServiceProvider(sanitizer: Sanitizer) {
  // set below to true to make alerts look like toast
  let isToast = false;
  return new AlertService(sanitizer, isToast, null);
}

@NgModule({
  imports: [
    NgbModule.forRoot(),
    CommonModule,
    NgJhipsterModule.forRoot({
      i18nEnabled: true, defaultI18nLang: 'de'
    }),
    SpinnerModule
  ],
  declarations: [
    JhiAlertComponent,
    JhiAlertErrorComponent,
    SpinnerWrapperComponent,
    SortPipe
  ],
  providers: [
    EventManager,
    DatePipe,
    DateUtils,
    CookieService,
    {
      provide: AlertService,
      useFactory: alertServiceProvider,
      deps: [Sanitizer]
    }
  ],
  entryComponents: [],
  exports: [
    FormsModule,
    JhiAlertComponent,
    JhiAlertErrorComponent,
    CommonModule,
    NgbModule,
    NgJhipsterModule,
    SpinnerWrapperComponent,
    SortPipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class SharedModule {
}
