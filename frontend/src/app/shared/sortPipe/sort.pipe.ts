import { Pipe } from '@angular/core';

@Pipe({name: "sortBy"})
export class SortPipe {
  transform(array: Array<string>, args: string, direction: string): Array<string> {
    direction = (direction == null || direction == '') ? 'asc' : direction;

    array.sort((a: any, b: any) => {
      if (a[args] < b[args]) {
        return -1;
      } else if (a[args] > b[args]) {
        return 1;
      } else {
        return 0;
      }
    });

    if (direction == 'desc') {
      array.reverse();
    }

    return array;
  }
}
