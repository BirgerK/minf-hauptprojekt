import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'spinner-wrapper',
  templateUrl: './spinner-wrapper.component.html',
  styleUrls: ['./spinner-wrapper.component.scss']
})
export class SpinnerWrapperComponent implements OnInit {
  @Input() spinning: boolean = true;

  constructor() { }

  ngOnInit() {
  }

}
