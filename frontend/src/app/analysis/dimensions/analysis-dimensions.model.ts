export class AnalysisDimensions {
  constructor(
    public rows: number,
    public columns: number) {
  }
}
