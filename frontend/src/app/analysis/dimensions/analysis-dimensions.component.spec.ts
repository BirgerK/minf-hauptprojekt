import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisDimensionsComponent } from './';

describe('AnalysisDimensionsComponent', () => {
  let component: AnalysisDimensionsComponent;
  let fixture: ComponentFixture<AnalysisDimensionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnalysisDimensionsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisDimensionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
