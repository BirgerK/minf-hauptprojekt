import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AnalysisService } from '../analysis.service';
import { AnalysisDimensions } from './analysis-dimensions.model';

@Component({
  selector: 'analysis-dimensions',
  templateUrl: './analysis-dimensions.component.html',
  styleUrls: ['./analysis-dimensions.component.scss']
})
export class AnalysisDimensionsComponent implements OnInit, OnDestroy, OnChanges {
  @Input() datasetId: number;
  loadingDimensions: boolean = false;
  dimensions: AnalysisDimensions;

  constructor(private analysisService: AnalysisService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('datasetId' in changes) {
      this.load(this.datasetId)
    }
  }

  load(id) {
    this.loadingDimensions = true;
    this.analysisService.getDimensions(id).subscribe(dimensions => {
      this.dimensions = dimensions;
      this.loadingDimensions = false;
    });
  }

  ngOnDestroy() {
  }

}
