import 'chart.js';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AnalysisService } from '../analysis.service';
import { AnalysisDataCorrelation, AnalysisDataCorrelationDetails } from './analysis-correlations.model';

@Component({
  selector: 'analysis-data-correlations',
  templateUrl: './analysis-correlations.component.html',
  styleUrls: ['./analysis-correlations.component.scss']
})
export class AnalysisDataCorrelationsComponent implements OnInit, OnDestroy, OnChanges {
  @Input() datasetId: number;
  loadingCorrelations: boolean = false;
  loadingCorrelationDetails: { [key: string]: boolean } = {};
  correlations: AnalysisDataCorrelation[];
  correlationsDetails: { [key: string]: AnalysisDataCorrelationDetails } = {};
  correlationIsShown: Object = {};

  public lineChartOptions: {
    [key: string]: { [key: string]: any }
  } = {};
  public lineChartType: string = 'scatter';
  public lineChartLegend: boolean = false;

  public lineChartData: { [key: string]: any[] } = {};

  constructor(private analysisService: AnalysisService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('datasetId' in changes) {
      this.load(this.datasetId)
    }
  }

  load(id) {
    this.loadingCorrelations = true;
    this.analysisService.getDataCorrelations(id).subscribe(correlations => {
      this.correlations = correlations;
      for (let correlation of this.correlations) {
        correlation.absoluteCorrelation = Math.abs(correlation.correlation);
      }
      this.loadingCorrelations = false;
    });
  }

  toggleDistribution(attribute1: string, attribute2: string) {
    let correlationName: string = this.buildCorrelationName(attribute1, attribute2);
    this.correlationIsShown[correlationName] = !this.correlationIsShown[correlationName];

    if (this.correlationIsShown[correlationName]) {
      if (!(correlationName in this.correlationsDetails)) {
        this.loadCorrelationDetails(attribute1, attribute2)
      }
    }
  }

  buildCorrelationName(attr1: string, attr2: string): string {
    return attr1 + '_' + attr2;
  }

  loadCorrelationDetails(attr1: string, attr2: string) {
    let correlationName: string = this.buildCorrelationName(attr1, attr2);

    this.loadingCorrelationDetails[correlationName] = true;

    this.analysisService.getDataCorrelationDetails(this.datasetId, attr1, attr2).subscribe(correlationDetails => {
      this.correlationsDetails[correlationName] = correlationDetails;

      let dataset = [];
      for (let i in correlationDetails.data1) {
        dataset.push({
          y: correlationDetails.data1[i],
          x: correlationDetails.data2[i]
        })
      }

      this.lineChartData[correlationName] = [
        {
          backgroundColor: '#ff0000',
          data: dataset
        }
      ];

      this.lineChartOptions[correlationName] = {
        showLines: false,
        scaleShowVerticalLines: false,
        responsive: true,
        datasetStroke: false,
        scales: {
          xAxes: [{
            scaleLabel: {
              labelString: correlationDetails.attribute1
            }
          }],
          yAxes: [{
            scaleLabel: {
              labelString: correlationDetails.attribute2
            }
          }]
        }
      };


      this.loadingCorrelationDetails[correlationName] = false;
    });
  }

  ngOnDestroy() {
    this.lineChartOptions = {};
    this.correlationsDetails = {};
  }

  public headerIsGreen(correlation: AnalysisDataCorrelation): boolean {
    let value: number = correlation.absoluteCorrelation;
    return value != null && value >= 0.7;
  }

  public headerIsYellow(correlation: AnalysisDataCorrelation): boolean {
    let value: number = correlation.absoluteCorrelation;
    return value != null && value >= 0.5 && value < 0.7;
  }

  public headerIsRed(correlation: AnalysisDataCorrelation): boolean {
    let value: number = correlation.absoluteCorrelation;
    return value != null && value >= 0 && value < 0.5;
  }

}
