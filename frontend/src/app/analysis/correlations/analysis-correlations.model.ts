export class AnalysisDataCorrelation {
  constructor(
    public attribute1: string,
    public attribute2: string,
    public correlation: number,
    public absoluteCorrelation: number) {
  }
}

export class AnalysisDataCorrelationDetails {
  constructor(
    public attribute1: string,
    public data1: string[],
    public attribute2: string,
    public data2: string[]) {
  }
}
