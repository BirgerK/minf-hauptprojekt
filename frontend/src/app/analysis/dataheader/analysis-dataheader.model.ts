export class AnalysisDataHeader {
  constructor(
    public column_headers: string[],
    public data_rows: Object[]) {
  }
}
