import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisDataHeaderComponent } from './';

describe('AnalysisDataHeaderComponent', () => {
  let component: AnalysisDataHeaderComponent;
  let fixture: ComponentFixture<AnalysisDataHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnalysisDataHeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisDataHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
