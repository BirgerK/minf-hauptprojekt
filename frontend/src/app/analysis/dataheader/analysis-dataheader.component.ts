import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnalysisService } from '../analysis.service';
import { AnalysisDataHeader } from './analysis-dataheader.model';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'analysis-dataheader',
  templateUrl: './analysis-dataheader.component.html',
  styleUrls: ['./analysis-dataheader.component.scss']
})
export class AnalysisDataHeaderComponent implements OnInit, OnDestroy, OnChanges {
  @Input() datasetId: number;
  dataheader: AnalysisDataHeader;
  tableSettings: Object;
  tableDataSource: LocalDataSource;
  loadingHeaders: boolean = false;
  private subscription: any;

  constructor(private analysisService: AnalysisService,
    private route: ActivatedRoute) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('datasetId' in changes) {
      this.load(this.datasetId)
    }
  }

  load(id) {
    this.loadingHeaders = true;
    this.analysisService.getDataHeader(id).subscribe(dataheader => {
      this.dataheader = dataheader;
      this.tableDataSource = new LocalDataSource(dataheader.data_rows);
      this.initTableSettings();
      this.loadingHeaders = false;
    });
  }

  initTableSettings() {
    let settings: Object = {
      columns: {},
      actions: {
        add: false,
        edit: false,
        delete: false
      },
      pager: {
        display: false
      }
    };

    for (let column of this.dataheader.column_headers) {
      let setting = {
        title: column,
        editable: false
      };
      settings['columns'][column] = setting;
    }

    this.tableSettings = settings;
  }

  ngOnDestroy() {
  }

}
