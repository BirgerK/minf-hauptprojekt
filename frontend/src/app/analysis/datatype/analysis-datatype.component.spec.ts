import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisDatatypeComponent } from './';

describe('AnalysisDatatypeComponent', () => {
  let component: AnalysisDatatypeComponent;
  let fixture: ComponentFixture<AnalysisDatatypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnalysisDatatypeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisDatatypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
