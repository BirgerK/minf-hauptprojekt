import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AnalysisService } from '../analysis.service';
import { AnalysisDatatype } from './analysis-datatype.model';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'analysis-datatype',
  templateUrl: './analysis-datatype.component.html',
  styleUrls: ['./analysis-datatype.component.scss']
})
export class AnalysisDatatypeComponent implements OnInit, OnDestroy, OnChanges {
  @Input() datasetId: number;
  loadingTypes: boolean = false;
  datatype: AnalysisDatatype;
  tableSettings: Object;
  tableDataSource: LocalDataSource;
  private subscription: any;

  constructor(private analysisService: AnalysisService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('datasetId' in changes) {
      this.load(this.datasetId)
    }
  }

  load(id) {
    this.loadingTypes = true;
    this.analysisService.getDatatype(id).subscribe(datatype => {
      this.datatype = datatype;
      this.tableDataSource = new LocalDataSource(datatype.columns_with_type);
      this.initTableSettings();
      this.loadingTypes = false;
    });
  }

  initTableSettings() {
    let settings: Object = {
      columns: {},
      actions: {
        add: false,
        edit: false,
        delete: false
      },
      pager: {
        display: false
      }
    };

    settings['columns']['column'] = {
      title: 'Attribut',
      editable: false
    };
    settings['columns']['type'] = {
      title: 'Typ',
      editable: false
    };

    this.tableSettings = settings;
  }

  ngOnDestroy() {
  }

}
