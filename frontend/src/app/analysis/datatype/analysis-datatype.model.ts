export class AnalysisDatatype {
  constructor(
    public columns_with_type: Datatype[]) {
  }
}

class Datatype {
  constructor(
    public column: string,
    public type: string) {
  }
}
