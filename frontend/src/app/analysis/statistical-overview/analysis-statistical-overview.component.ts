import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnalysisService } from '../analysis.service';
import { AnalysisStatisticalOverview } from './analysis-statistical-overview.model';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'analysis-statistical-overview',
  templateUrl: './analysis-statistical-overview.component.html',
  styleUrls: ['./analysis-statistical-overview.component.scss']
})
export class AnalysisStatisticalOverviewComponent implements OnInit, OnDestroy, OnChanges {
  @Input() datasetId: number;
  loadingOverview: boolean = true;
  statisticalOverview: AnalysisStatisticalOverview;
  tableSettings: Object;
  tableDataSource: LocalDataSource;
  private subscription: any;

  constructor(private analysisService: AnalysisService,
    private route: ActivatedRoute) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('datasetId' in changes) {
      this.load(this.datasetId)
    }
  }

  load(id) {
    this.loadingOverview = true;
    this.analysisService.getStatisticalOverview(id).subscribe(statisticalOverview => {
      this.statisticalOverview = statisticalOverview;
      this.tableDataSource = new LocalDataSource(statisticalOverview.rows);
      this.initTableSettings();
      this.loadingOverview = false;
    });
  }

  initTableSettings() {
    let settings: Object = {
      columns: {},
      actions: {
        add: false,
        edit: false,
        delete: false
      },
      pager: {
        display: false
      }
    };

    settings['columns']['title'] = {
      title: 'Titel',
      editable: false
    };
    settings['columns']['min'] = {
      title: 'Min',
      editable: false
    };
    settings['columns']['first_percentile'] = {
      title: '25th Percentile',
      editable: false
    };
    settings['columns']['median'] = {
      title: 'Median',
      editable: false
    };
    settings['columns']['mean'] = {
      title: 'Mean',
      editable: false
    };
    settings['columns']['third_percentile'] = {
      title: '75th Percentile',
      editable: false
    };
    settings['columns']['max'] = {
      title: 'Max',
      editable: false
    };
    settings['columns']['standard_deviation'] = {
      title: 'Standard-Abweichung',
      editable: false
    };

    this.tableSettings = settings;
  }

  ngOnDestroy() {
  }

}
