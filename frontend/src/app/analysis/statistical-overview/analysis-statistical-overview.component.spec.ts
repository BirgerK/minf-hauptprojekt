import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisStatisticalOverviewComponent } from './analysis-statistatical-overview.component';

describe('AnalysisStatisticalOverviewComponent', () => {
  let component: AnalysisStatisticalOverviewComponent;
  let fixture: ComponentFixture<AnalysisStatisticalOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalysisStatisticalOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisStatisticalOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
