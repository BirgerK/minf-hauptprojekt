export class AnalysisStatisticalOverview {
  constructor(
    public rows: StatisticalRow[]) {
  }
}

class StatisticalRow {
  constructor(
    public title: string,
    public min: number,
    public first_percentile: number,
    public median: number,
    public mean: number,
    public third_percentile: number,
    public max: number,
    public standard_deviation: number) {
  }
}
