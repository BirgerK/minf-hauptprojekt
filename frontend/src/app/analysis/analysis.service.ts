import { Injectable } from '@angular/core';
import {
  Http,
  Response,
  URLSearchParams,
  BaseRequestOptions,
  ResponseContentType,
  Headers,
  RequestMethod
} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { AnalysisDimensions, AnalysisDatatype, AnalysisDataHeader, AnalysisStatisticalOverview, AnalysisDataDistribution, AnalysisDataCorrelation, AnalysisDataCorrelationDetails } from './';
import { DateUtils } from 'ng-jhipster';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
@Injectable()
export class AnalysisService {

  private resourceUrl = environment.backendUrl + 'api/dataset';

  constructor(private http: Http, private dateUtils: DateUtils, private sanitizer: DomSanitizer) {
  }

  getDimensions(datasetId: number): Observable<AnalysisDimensions> {
    return this.http.get(`${this.resourceUrl}/${datasetId}/dimensions`)
      .map((res: Response) => {
        let jsonResponse = res.json();
        return jsonResponse;
      })
      ;
  }

  getDatatype(datasetId: number): Observable<AnalysisDatatype> {
    return this.http.get(`${this.resourceUrl}/${datasetId}/types`)
      .map((res: Response) => {
        let jsonResponse = res.json();
        return jsonResponse;
      })
      ;
  }

  getDataHeader(datasetId: number): Observable<AnalysisDataHeader> {
    return this.http.get(`${this.resourceUrl}/${datasetId}/header`)
      .map((res: Response) => {
        let jsonResponse = res.json();
        return jsonResponse;
      })
      ;
  }

  getStatisticalOverview(datasetId: number): Observable<AnalysisStatisticalOverview> {
    return this.http.get(`${this.resourceUrl}/${datasetId}/statistical-overview`)
      .map((res: Response) => {
        let jsonResponse = res.json();
        return jsonResponse;
      })
      ;
  }

  getDataDistribution(datasetId: number): Observable<AnalysisDataDistribution> {
    return this.http.get(`${this.resourceUrl}/${datasetId}/distributions`)
      .map((res: Response) => {
        let jsonResponse = res.json();
        return jsonResponse;
      })
      ;
  }

  getDataCorrelations(datasetId: number): Observable<AnalysisDataCorrelation[]> {
    return this.http.get(`${this.resourceUrl}/${datasetId}/correlations`)
      .map((res: Response) => {
        let jsonResponse = res.json();
        return jsonResponse;
      })
      ;
  }

  getDataCorrelationDetails(datasetId: number, attribute1: string, attribute2: string): Observable<AnalysisDataCorrelationDetails> {
    return this.http.get(`${this.resourceUrl}/${datasetId}/correlations/${attribute1}/${attribute2}`)
      .map((res: Response) => {
        let jsonResponse = res.json();
        return jsonResponse;
      })
      ;
  }

  private convertResponse(res: any): any {
    let jsonResponse = res.json();
    res._body = jsonResponse;
    return res;
  }
}
