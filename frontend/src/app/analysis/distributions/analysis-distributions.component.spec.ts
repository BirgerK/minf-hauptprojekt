import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisDataDistributionComponent } from './';

describe('AnalysisDataDistributionComponent', () => {
  let component: AnalysisDataDistributionComponent;
  let fixture: ComponentFixture<AnalysisDataDistributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnalysisDataDistributionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisDataDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
