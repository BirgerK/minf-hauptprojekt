import 'chart.js';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnalysisService } from '../analysis.service';
import { AnalysisDataDistribution, ColumnDistribution, DistributionCount } from './analysis-distributions.model';

@Component({
  selector: 'analysis-data-distribution',
  templateUrl: './analysis-distributions.component.html',
  styleUrls: ['./analysis-distributions.component.scss']
})
export class AnalysisDataDistributionComponent implements OnInit, OnDestroy, OnChanges {
  @Input() datasetId: number;
  loadingDistributions: boolean = false;
  dataDistribution: AnalysisDataDistribution;
  tableSettings: Object;
  distributionIsShown: Object = {};
  private subscription: any;

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      xAxes: [{
        categoryPercentage: 1.0,
        barPercentage: 1.0
      }]
    }
  };
  public barChartLabels: { [key: string]: string[] } = {};
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;

  public barChartData: { [key: string]: any[] } = {};

  constructor(private analysisService: AnalysisService,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('datasetId' in changes) {
      this.load(this.datasetId)
    }
  }

  load(id) {
    this.loadingDistributions = true;
    this.analysisService.getDataDistribution(id).subscribe(dataDistribution => {
      this.dataDistribution = dataDistribution;
      for (let columnDistribution of this.dataDistribution.distributions_with_column) {
        columnDistribution.absoluteSkewness = Math.abs(columnDistribution.skewness);
        let labels: string[] = columnDistribution.distribution.map((dist: DistributionCount) => {
          return dist.value;
        });
        this.barChartLabels[columnDistribution.column] = labels;

        let dataValues: number[] = columnDistribution.distribution.map((dist: DistributionCount) => {
          return dist.count;
        });
        this.barChartData[columnDistribution.column] = [
          {
            data: dataValues
          }
        ]
      }
      this.loadingDistributions = false;
    });
  }

  toggleDistribution(columnName) {
    this.distributionIsShown[columnName] = !this.distributionIsShown[columnName];
  }

  ngOnDestroy() {
  }


  public headerIsGreen(distribution: ColumnDistribution): boolean {
    let value: number = distribution.absoluteSkewness;
    return value != null && value <= 0.7;
  }

  public headerIsYellow(distribution: ColumnDistribution): boolean {
    let value: number = distribution.absoluteSkewness;
    return value != null && value > 0.7 && value <= 1.5;
  }

  public headerIsRed(distribution: ColumnDistribution): boolean {
    let value: number = distribution.absoluteSkewness;
    return value != null && value > 1.5;
  }

}
