export class AnalysisDataDistribution {
  constructor(
    public distributions_with_column: ColumnDistribution[]) {
  }
}

export class ColumnDistribution {
  constructor(
    public column: string,
    public skewness: number,
    public absoluteSkewness: number,
    public distribution: DistributionCount[]) {
  }
}

export class DistributionCount {
  constructor(
    public value: string,
    public count: number
  ) {
  }
}
