import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import {
  AnalysisDataCorrelationsComponent,
  AnalysisDataDistributionComponent,
  AnalysisDataHeaderComponent,
  AnalysisDatatypeComponent,
  AnalysisDimensionsComponent,
  AnalysisService,
  AnalysisStatisticalOverviewComponent
} from './';
import { SharedModule } from '../shared/shared.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ChartsModule } from 'ng2-charts';
@NgModule({
  imports: [
    SharedModule,
    Ng2SmartTableModule,
    ChartsModule
  ],
  declarations: [
    AnalysisDimensionsComponent,
    AnalysisDatatypeComponent,
    AnalysisDataHeaderComponent,
    AnalysisStatisticalOverviewComponent,
    AnalysisDataDistributionComponent,
    AnalysisDataCorrelationsComponent
  ],
  entryComponents: [],
  providers: [AnalysisService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    AnalysisDimensionsComponent,
    AnalysisDatatypeComponent,
    AnalysisDataHeaderComponent,
    AnalysisStatisticalOverviewComponent,
    AnalysisDataDistributionComponent,
    AnalysisDataCorrelationsComponent
  ]
})
export class AnalysisModule {
}
