import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { HttpModule, Headers, BaseRequestOptions, RequestOptions, XSRFStrategy, CookieXSRFStrategy } from '@angular/http';

import { AppComponent } from './app.component';

import { DatasetModule } from './dataset/dataset.module';
import { AnalysisModule } from './analysis/analysis.module';
import { HomeComponent } from './home/home.component';
import { SharedModule} from './shared/shared.module';

import { homeRoute } from './home';
import { ChartsModule } from 'ng2-charts';

export function translateLoader(cookieService: any) { return new CookieXSRFStrategy('csrftoken', 'X-CSRFToken'); }

let LAYOUT_ROUTES = [
  homeRoute
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(LAYOUT_ROUTES, { useHash: true }),
    SharedModule,
    DatasetModule,
    AnalysisModule,
    ChartsModule
  ],
  providers: [
    CookieService,
    {
      provide: XSRFStrategy,
      useFactory: translateLoader,
      deps: [CookieService]
    }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
