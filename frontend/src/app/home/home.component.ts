import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';
import { SharedModule} from '../shared/shared.module';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: [
    'home.scss'
  ]

})
export class HomeComponent implements OnInit {

  constructor(private jhiLanguageService: JhiLanguageService,
    private eventManager: EventManager) {
  }

  ngOnInit() {
  }
}
