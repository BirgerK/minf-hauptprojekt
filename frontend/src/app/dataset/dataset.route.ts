import { Routes } from '@angular/router';

import { DatasetUploadComponent } from './upload/dataset-upload.component';
import { DatasetIndexComponent } from './index/dataset-index.component';
import { DatasetShowComponent } from './show/dataset-show.component';


export const datasetRoute: Routes = [
  {
    path: 'dataset/:id',
    component: DatasetShowComponent
  }
];
