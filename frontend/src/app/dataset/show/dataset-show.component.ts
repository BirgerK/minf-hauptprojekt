import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { DatasetService } from '../dataset.service';
import { Dataset } from '../dataset.model';
import { Subscription } from 'rxjs/Rx';
import { EventManager, AlertService } from 'ng-jhipster';

@Component({
  selector: 'app-dataset-show',
  templateUrl: './dataset-show.component.html',
  styleUrls: ['./dataset-show.component.scss']
})
export class DatasetShowComponent implements OnInit {
  dataset: Dataset;
  private subscription: any;

  constructor(private datasetService: DatasetService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(params => {
      this.load(params['id']);
    });
  }

  load(id) {
    this.datasetService.find(id).subscribe(dataset => {
      this.dataset = dataset;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
