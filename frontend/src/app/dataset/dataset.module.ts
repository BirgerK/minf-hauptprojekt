import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatasetUploadComponent, DatasetIndexComponent,
  DatasetShowComponent, DatasetService, datasetRoute,
  Dataset } from './';
import { FileUploadModule } from 'ng2-file-upload';
import { SharedModule} from '../shared/shared.module';
import { AnalysisModule } from '../analysis/analysis.module';

let ENTITY_STATES = [
  ...datasetRoute,
];

@NgModule({
  imports: [
    FileUploadModule,
    RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
    SharedModule,
    AnalysisModule
  ],
  declarations: [
    DatasetUploadComponent,
    DatasetIndexComponent,
    DatasetShowComponent
  ],
  entryComponents: [
    DatasetUploadComponent,
    DatasetIndexComponent,
    DatasetShowComponent
  ],
  providers: [
    DatasetService,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    DatasetUploadComponent,
    DatasetIndexComponent,
    DatasetShowComponent
  ]
})
export class DatasetModule {
}
