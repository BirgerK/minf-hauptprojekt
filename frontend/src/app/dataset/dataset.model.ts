export class Dataset {
  constructor(public id: number,
    public name: string,
    public path: string) {
  }
}
