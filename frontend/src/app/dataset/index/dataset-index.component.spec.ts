import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasetIndexComponent } from './dataset-index.component';

describe('DatasetIndexComponent', () => {
  let component: DatasetIndexComponent;
  let fixture: ComponentFixture<DatasetIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasetIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
