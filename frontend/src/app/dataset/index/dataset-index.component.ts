import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { DatasetService } from '../dataset.service';
import { Dataset } from '../dataset.model';
import { Subscription } from 'rxjs/Rx';
import { EventManager, AlertService } from 'ng-jhipster';

@Component({
  selector: 'app-dataset-index',
  templateUrl: './dataset-index.component.html',
  styleUrls: ['./dataset-index.component.scss']
})
export class DatasetIndexComponent implements OnInit, OnDestroy {
  datasets: Dataset[];
  eventSubscriber: Subscription;

  constructor(private datasetService: DatasetService,
    private alertService: AlertService,
    private eventManager: EventManager) { }

  loadAll() {
    this.datasetService.query().subscribe(
      (res: Response) => {
        this.datasets = res.json();
      },
      (res: Response) => this.onError(res.json())
    );
  }

  delete(id: number) {
    this.datasetService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'datasetListModification',
        content: 'Deleted a dataset'
      });
    });
  }
  ngOnInit() {
    this.loadAll();
    this.registerChangeInDatasets();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: Dataset) {
    return item.id;
  }


  registerChangeInDatasets() {
    this.eventSubscriber = this.eventManager.subscribe('datasetListModification', (response) => this.loadAll());
  }


  private onError(error) {
    this.alertService.error(error.message, null, null);
  }

}
