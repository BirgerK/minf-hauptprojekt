import { Component, OnInit } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { environment } from '../../../environments/environment';
import { CookieService } from 'angular2-cookie/core';
import { EventManager } from 'ng-jhipster';

const FILE_UPLOAD_URL = environment.backendUrl + 'api/dataset/';

@Component({
  selector: 'app-dataset-upload',
  templateUrl: './dataset-upload.component.html',
  styleUrls: ['./dataset-upload.component.scss']
})
export class DatasetUploadComponent implements OnInit {

  datasetName: string = '';
  uploader: FileUploader = new FileUploader({
    url: FILE_UPLOAD_URL,
    itemAlias: 'file',
    isHTML5: true,
    headers: [
      { name: 'X-CSRFToken', value: this.getCookie('csrftoken') }
    ]
  });

  constructor(private _cookieService: CookieService,
    private eventManager: EventManager) { }

  ngOnInit() {
    this.uploader.onBeforeUploadItem = (item: FileItem) => {
      this.uploader.options.additionalParameter = {
        name: this.datasetName
      };
      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        this.eventManager.broadcast({
          name: 'datasetListModification',
          content: 'Deleted a dataset'
        });
      };
    };

  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }

}
