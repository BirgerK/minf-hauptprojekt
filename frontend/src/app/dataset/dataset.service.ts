import { Injectable } from '@angular/core';
import {
  Http,
  Response,
  URLSearchParams,
  BaseRequestOptions,
  ResponseContentType,
  Headers,
  RequestMethod
} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Dataset } from './dataset.model';
import { DateUtils } from 'ng-jhipster';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
@Injectable()
export class DatasetService {

  private resourceUrl = environment.backendUrl + 'api/dataset';

  constructor(private http: Http, private dateUtils: DateUtils, private sanitizer: DomSanitizer) {
  }

  update(dataset: Dataset): Observable<Dataset> {
    let copy: Dataset = Object.assign({}, dataset);
    return this.http.put(`${this.resourceUrl}/${dataset.id}/`, copy).map((res: Response) => {
      return res.json();
    });
  }

  find(id: number): Observable<Dataset> {
    return this.http.get(`${this.resourceUrl}/${id}/`).map((res: Response) => {
      let jsonResponse = res.json();
      return jsonResponse;
    });
  }

  query(req?: any): Observable<Response> {
    let options = this.createRequestOption(req);
    return this.http.get(`${this.resourceUrl}/`, options)
      .map((res: any) => this.convertResponse(res))
      ;
  }

  delete(id: number): Observable<Response> {
    return this.http.delete(`${this.resourceUrl}/${id}/`);
  }


  private convertResponse(res: any): any {
    let jsonResponse = res.json();
    res._body = jsonResponse;
    return res;
  }

  private createRequestOption(req?: any): BaseRequestOptions {
    let options: BaseRequestOptions = new BaseRequestOptions();
    if (req) {
      let params: URLSearchParams = new URLSearchParams();
      params.set('page', req.page);
      params.set('size', req.size);
      if (req.sort) {
        params.paramsMap.set('sort', req.sort);
      }
      params.set('query', req.query);

      options.search = params;
    }
    return options;
  }
}
