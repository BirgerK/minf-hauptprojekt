from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    # path: /dataset/
    url(r'^dataset/$', views.DatasetListResource.as_view()),
    url(r'^dataset/(?P<pk>[0-9]+)/$', views.DatasetDetailsResource.as_view()),
    url(r'^dataset/(?P<pk>[0-9]+)/dimensions$',
        views.DimensionsResource.as_view()),
    url(r'^dataset/(?P<pk>[0-9]+)/types$',
        views.DataTypeResource.as_view()),
    url(r'^dataset/(?P<pk>[0-9]+)/header$',
        views.DataHeadResource.as_view()),
    url(r'^dataset/(?P<pk>[0-9]+)/statistical-overview$',
        views.StatisticalOverviewResource.as_view()),
    url(r'^dataset/(?P<pk>[0-9]+)/distributions$',
        views.DataDistributionResource.as_view()),
    url(r'^dataset/(?P<pk>[0-9]+)/correlations$',
        views.DataCorrelationResource.as_view()),
    url(r'^dataset/(?P<pk>[0-9]+)/correlations/(?P<attr1>\w+)/(?P<attr2>\w+)$',
        views.DataCorrelationDetailsResource.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
