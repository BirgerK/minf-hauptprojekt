import json

from django.db import models
from django.forms.models import model_to_dict

class Dataset(models.Model):
    name = models.CharField(max_length=200)
    path = models.CharField(max_length=500)

    def __str__(self):
        dict_obj = model_to_dict( self )
        return json.dumps(dict_obj)
