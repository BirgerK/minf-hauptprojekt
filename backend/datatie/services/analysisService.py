import logging

import numpy as np
import pandas as pd

import datatie.dtos as dtos
from datatie.exceptions import NotDatasetGivenException
from . import datasetService

logger = logging.getLogger(__name__)


def get_dimensions(dataset):
    logger.debug('Get dimensions for dataset')
    if not dataset:
        raise NotDatasetGivenException("No dataset given")

    result = dtos.DimensionsDto()
    logger.debug('Dataset for dimension-extraction is id: ' + str(dataset.id))
    df = datasetService.readDataset(dataset)
    result.rows = df.shape[0]
    result.columns = df.shape[1]
    return result


def get_datatypes(dataset):
    logger.debug('Get datatypes for dataset')
    if not dataset:
        raise NotDatasetGivenException("No dataset given")

    result = dtos.DataTypeDto()
    logger.debug('Dataset for datatype-extraction is id: ' + str(dataset.id))
    df = datasetService.readDataset(dataset)
    for column, dtype in df.dtypes.iteritems():
        result.add(column, str(dtype))
    return result


def get_dataheader(dataset, header_size=10):
    logger.debug('Get dataheader for dataset')
    if not dataset:
        raise NotDatasetGivenException("No dataset given")

    result = dtos.DataHeadDto()
    logger.debug('Dataset for dataheader-extraction is id: ' + str(dataset.id))
    df = datasetService.readDataset(dataset)
    result.column_headers = df.columns.tolist()
    result.data_rows = df.head(header_size).fillna(
        'NaN').to_dict(orient='records')
    return result


def get_statistical_overview(dataset):
    logger.debug('Get statistical-overview for dataset')
    if not dataset:
        raise NotDatasetGivenException("No dataset given")

    result = dtos.StatisticalOverviewDto()
    logger.debug('Dataset for statistical-overview is id: ' + str(dataset.id))
    df = datasetService.readDataset(dataset)
    df_numeric = __extract_numeric_columns(df)
    for col in df_numeric.columns.tolist():
        s = df_numeric[col]
        col_statistics = {
            'title': col
        }
        col_statistics['min'] = __replace_nan(s.min())
        col_statistics['25th_percentile'] = __replace_nan(
            __calc_percentile(s, .25))
        col_statistics['median'] = __replace_nan(s.median())
        col_statistics['mean'] = __replace_nan(s.mean())
        col_statistics['75th_percentile'] = __replace_nan(
            __calc_percentile(s, .75))
        col_statistics['max'] = __replace_nan(s.max())
        col_statistics['standard_deviation'] = __replace_nan(s.std())

        result.add(col_statistics)

    return result


def get_data_distributions(dataset):
    logger.debug('Get distributions for dataset')
    if not dataset:
        raise NotDatasetGivenException("No dataset given")

    result = dtos.DataDistributionDto()
    logger.debug(
        'Dataset for distributions-extraction is id: ' + str(dataset.id))
    df = datasetService.readDataset(dataset)

    columns = df.columns.tolist()
    numeric_columns = __extract_numeric_columns(df).columns.tolist()
    for column in columns:
        distributions = []
        value_counts = df[column].value_counts()
        for value in value_counts.index.tolist():
            distribution = {
                'value': value,
                'count': value_counts[value]
            }
            distributions.append(distribution)

        if column in numeric_columns:
            skewness = __replace_nan(df[column].skew())
        else:
            skewness = 'None'
        distributions = sorted(distributions, key=lambda x: x['value'])
        result.add(column, skewness, distributions)
    return result


def get_data_correlations(dataset):
    logger.debug('Get correlations for dataset')
    if not dataset:
        raise NotDatasetGivenException("No dataset given")

    result = []
    logger.debug(
        'Dataset for correlations-extraction is id: ' + str(dataset.id))
    df = datasetService.readDataset(dataset)

    df_corr = df.corr()

    columns = df_corr.columns.tolist()
    indices = df_corr.index.tolist()
    for column in columns:
        for index in indices:
            if index != column and not __contains_correlation_with_attributes(result, index, column):
                correlation = dtos.CorrelationDto()
                correlation.attribute1 = column
                correlation.attribute2 = index
                correlation.correlation = __replace_nan(df_corr.loc[index, column])
                result.append(correlation)

    return result


def get_data_correlation(dataset, attribute1, attribute2):
    logger.debug('Get correlation-details for dataset')
    if not dataset:
        raise NotDatasetGivenException("No dataset given")

    logger.debug(
        'Dataset for correlation-details-extraction is id: ' + str(dataset.id))
    df = datasetService.readDataset(dataset)

    result = dtos.CorrelationDetailsDto()
    result.attribute1 = attribute1
    result.data1 = [__replace_nan(x) for x in df[attribute1].values.tolist()]
    result.attribute2 = attribute2
    result.data2 = [__replace_nan(x) for x in df[attribute2].values.tolist()]

    return result

###################
##### PRIVATE #####
###################


def __extract_numeric_columns(df):
    return df.select_dtypes(include=[np.number])


def __calc_percentile(s, quantile):
    return s.quantile(quantile)


def __replace_nan(obj):
    if(pd.isnull(obj)):
        return 'NaN'
    else:
        return obj


def __contains_correlation_with_attributes(correlations, attribute1, attribute2):
    for correlation in correlations:
        if (correlation.attribute1 == attribute1 and correlation.attribute2 == attribute2) or \
                (correlation.attribute1 == attribute2 and correlation.attribute2 == attribute1):
            return True
    return False
