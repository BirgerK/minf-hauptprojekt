from rest_framework import serializers

from datatie.dtos import DimensionsDto, DataTypeDto, DataHeadDto, StatisticalOverviewDto, CorrelationDto, \
    DataDistributionDto, CorrelationDetailsDto
from datatie.models import Dataset


class DatasetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dataset
        fields = ('id', 'name', 'path')


class DimensionsSerializer(serializers.Serializer):
    rows = serializers.IntegerField()
    columns = serializers.IntegerField()

    def create(self, validated_data):
        return DimensionsDto(id=None, **validated_data)


class DataTypeSerializer(serializers.Serializer):
    columns_with_type = serializers.ListField()

    def create(self, validated_data):
        return DataTypeDto(id=None, **validated_data)


class DataHeadSerializer(serializers.Serializer):
    column_headers = serializers.ListField()
    data_rows = serializers.ListField()

    def create(self, validated_data):
        return DataHeadDto(id=None, **validated_data)


class StatisticalOverviewSerializer(serializers.Serializer):
    rows = serializers.ListField()

    def create(self, validated_data):
        return StatisticalOverviewDto(id=None, **validated_data)


class DataDistributionSerializer(serializers.Serializer):
    distributions_with_column = serializers.ListField()

    def create(self, validated_data):
        return DataDistributionDto(id=None, **validated_data)


class DataCorrelationSerializer(serializers.Serializer):
    attribute1 = serializers.CharField()
    attribute2 = serializers.CharField()
    correlation = serializers.CharField()

    def create(self, validated_data):
        return CorrelationDto(id=None, **validated_data)


class DataCorrelationDetailsSerializer(serializers.Serializer):
    attribute1 = serializers.CharField()
    attribute2 = serializers.CharField()
    data1 = serializers.ListField()
    data2 = serializers.ListField()

    def create(self, validated_data):
        return CorrelationDetailsDto(id=None, **validated_data)
