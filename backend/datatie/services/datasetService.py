import os
import logging
import uuid

import pandas as pd
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from datatie.models import Dataset

logger = logging.getLogger(__name__)
fs = FileSystemStorage()


def newDataset(name, file=None):
    dataset = Dataset(name=name)
    if file:
        path = __create_dataset_path()
        fs.save(path, file)
        dataset.path = path

    dataset.save()
    logger.debug('Created new dataset: ' + str(dataset))
    return dataset


def findAll():
    return Dataset.objects.all()


def readDataset(dataset):
    return pd.read_csv(dataset.path, sep=',', index_col=False)


###################
##### PRIVATE #####
###################


def __create_dataset_path():
    path = settings.TIE_DATA_PATH

    os.makedirs(path, exist_ok=True)

    unique_id = str(uuid.uuid4())
    path = os.path.join(path, unique_id + '.csv')

    return path
