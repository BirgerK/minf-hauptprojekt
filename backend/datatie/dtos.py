
class AbstractDto(object):

    def __str__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class DimensionsDto(AbstractDto):

    def __init__(self):
        self.rows = None
        self.columns = None


class DataTypeDto(AbstractDto):

    def __init__(self):
        self.columns_with_type = []

    def add(self, column, type):
        column_with_type = {'column': column, 'type': type}
        self.columns_with_type.append(column_with_type)


class DataHeadDto(AbstractDto):

    def __init__(self):
        self.column_headers = None
        self.data_rows = None


class StatisticalOverviewDto(AbstractDto):

    def __init__(self):
        self.rows = []

    def add(self, row):
        self.rows.append(row)


class DataDistributionDto(AbstractDto):

    def __init__(self):
        self.distributions_with_column = []

    def add(self, column, skewness, distribution):
        distribution_with_column = {
            'column': column,
            'skewness': skewness,
            'distribution': distribution
        }
        self.distributions_with_column.append(distribution_with_column)


class CorrelationDto(AbstractDto):

    def __init__(self):
        self.attribute1 = ''
        self.attribute2 = ''
        self.correlation = 0


class CorrelationDetailsDto(AbstractDto):

    def __init__(self):
        self.attribute1 = ''
        self.attribute2 = ''
        self.data1 = []
        self.data2 = []
