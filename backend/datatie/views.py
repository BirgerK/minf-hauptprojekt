import logging

from .models import Dataset
import datatie.services.serializers as serializers
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

from .services import datasetService, analysisService

logger = logging.getLogger(__name__)


def get_dataset(pk):
    try:
        return Dataset.objects.get(pk=pk)
    except Dataset.DoesNotExist:
        raise Http404


class DatasetListResource(generics.ListCreateAPIView):
    logger = logging.getLogger(__name__)
    queryset = datasetService.findAll()
    serializer_class = serializers.DatasetSerializer

    def post(self, request, *args, **kwargs):
        logger.debug(request.data)
        try:
            datasetName = request.data['name']
            uploadedFile = request.FILES['file']
            newDataset = datasetService.newDataset(
                datasetName, file=uploadedFile)
            serialized = serializers.DatasetSerializer(newDataset)
            return Response(serialized.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            self.logger.error('Error while uploading dataset: ' + str(e))
            return Response(status=status.HTTP_400_BAD_REQUEST)


class DatasetDetailsResource(generics.RetrieveUpdateDestroyAPIView):
    logger = logging.getLogger(__name__)
    queryset = datasetService.findAll()
    serializer_class = serializers.DatasetSerializer


class DimensionsResource(APIView):
    logger = logging.getLogger(__name__)
    serializer_class = serializers.DimensionsSerializer

    def get(self, request, pk, format=None):
        dataset = get_dataset(pk)
        dto = analysisService.get_dimensions(dataset)
        serializer = self.serializer_class(dto)
        return Response(serializer.data)


class DataTypeResource(APIView):
    logger = logging.getLogger(__name__)
    serializer_class = serializers.DataTypeSerializer

    def get(self, request, pk, format=None):
        dataset = get_dataset(pk)
        dto = analysisService.get_datatypes(dataset)
        serializer = self.serializer_class(dto)
        return Response(serializer.data)


class DataHeadResource(APIView):
    logger = logging.getLogger(__name__)
    serializer_class = serializers.DataHeadSerializer

    def get(self, request, pk, format=None):
        dataset = get_dataset(pk)
        dto = analysisService.get_dataheader(dataset)
        serializer = self.serializer_class(dto)
        return Response(serializer.data)


class StatisticalOverviewResource(APIView):
    logger = logging.getLogger(__name__)
    serializer_class = serializers.StatisticalOverviewSerializer

    def get(self, request, pk, format=None):
        dataset = get_dataset(pk)
        dto = analysisService.get_statistical_overview(dataset)
        serializer = self.serializer_class(dto)
        return Response(serializer.data)


class DataDistributionResource(APIView):
    logger = logging.getLogger(__name__)
    serializer_class = serializers.DataDistributionSerializer

    def get(self, request, pk, format=None):
        dataset = get_dataset(pk)
        dto = analysisService.get_data_distributions(dataset)
        serializer = self.serializer_class(dto)
        return Response(serializer.data)


class DataCorrelationResource(APIView):
    logger = logging.getLogger(__name__)
    serializer_class = serializers.DataCorrelationSerializer

    def get(self, request, pk, format=None):
        dataset = get_dataset(pk)
        result_dtos = analysisService.get_data_correlations(dataset)
        serializer = self.serializer_class(result_dtos, many=True)
        return Response(serializer.data)


class DataCorrelationDetailsResource(APIView):
    logger = logging.getLogger(__name__)
    serializer_class = serializers.DataCorrelationDetailsSerializer

    def get(self, request, pk, attr1, attr2, format=None):
        dataset = get_dataset(pk)
        result_dto = analysisService.get_data_correlation(
            dataset, attr1, attr2)
        serializer = self.serializer_class(result_dto)
        return Response(serializer.data)
