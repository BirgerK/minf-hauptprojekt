import os.path
import json

from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase
from rest_framework.test import APIRequestFactory

from ..views import DatasetListResource


class ApiDataset(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.testee = DatasetListResource.as_view()
        self.factory = APIRequestFactory()
        self.dataset_name = 'FIRST_DATASET'

    def test_upload_dataset(self):
        with open('datatie/tests/resources/simple_dataset.csv') as file:
            request = self.factory.post(
                '/dataset/', {'name': self.dataset_name, 'file': file}, format='multipart')
        request.user = AnonymousUser()

        response = self.testee(request)

        self.assertEqual(response.status_code, 201)

        datasetPath = response.data['path']
        self.assertTrue(datasetPath)
        self.assertTrue(os.path.isfile(datasetPath))
