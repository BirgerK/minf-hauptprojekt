from django.test import TestCase

import datatie.dtos as dtos
from datatie.services import analysisService
from . import utils


class TestAnalysisService(TestCase):
    def setUp(self):
        self.dataset = utils.create_simple_dataset()
        self.dataset_with_discrete_values = utils.create_simple_dataset_with_discrete_values()

    def test_get_dimensions(self):
        dimensions = analysisService.get_dimensions(self.dataset)

        self.assertEqual(dimensions.rows, 3)
        self.assertEqual(dimensions.columns, 2)

    def test_get_data_types(self):
        dto = analysisService.get_datatypes(self.dataset)

        self.assertEqual(len(dto.columns_with_type), 2)

        expectedCol1 = {'column': 'header0', 'type': 'int64'}
        expectedCol2 = {'column': 'header1', 'type': 'int64'}

        self.assertTrue(expectedCol1 in dto.columns_with_type)
        self.assertTrue(expectedCol2 in dto.columns_with_type)

    def test_get_data_headers(self):
        dto = analysisService.get_dataheader(self.dataset)

        expected_columns = ['header0', 'header1']
        expected_data_rows = [{
            'header0': 1,
            'header1': 2
        }, {
            'header0': 3,
            'header1': 4
        }, {
            'header0': 5,
            'header1': 6
        }]

        self.assertEqual(dto.column_headers, expected_columns)
        self.assertTrue(dto.data_rows, expected_data_rows)

    def test_get_statistical_overview(self):
        dto = analysisService.get_statistical_overview(self.dataset)

        self.assertEqual(len(dto.rows), 2)

        expected_attributes = ['title', 'min', '25th_percentile', 'median',
                               'mean', '75th_percentile', 'max', 'standard_deviation']
        self.assertEqual(set(dto.rows[0].keys()), set(expected_attributes))

    def test_get_data_distributions(self):
        dto = analysisService.get_data_distributions(
            self.dataset_with_discrete_values)

        self.assertEqual(len(dto.distributions_with_column), 3)

        expectedCol1 = {'column': 'header0', 'distribution': [
            {
                'value': 1,
                'count': 1
            },
            {
                'value': 3,
                'count': 1
            },
            {
                'value': 5,
                'count': 1
            }
        ], 'skewness': 0.0}
        expectedCol2 = {'column': 'header1', 'distribution': [
            {
                'value': 2,
                'count': 1
            },
            {
                'value': 4,
                'count': 1
            },
            {
                'value': 6,
                'count': 1
            }
        ], 'skewness': 0.0}
        expectedCol3 = {'column': 'class0', 'distribution': [
            {
                'value': 'a',
                'count': 1
            },
            {
                'value': 'b',
                'count': 2
            }
        ], 'skewness': 'None'}

        self.assertTrue(expectedCol1 in dto.distributions_with_column)
        self.assertTrue(expectedCol2 in dto.distributions_with_column)
        self.assertTrue(expectedCol3 in dto.distributions_with_column)

    def test_get_data_correlations(self):
        result_dtos = analysisService.get_data_correlations(self.dataset)
        self.assertEqual(len(result_dtos), 1)

        expectedCorrelation = dtos.CorrelationDto()
        expectedCorrelation.attribute1 = 'header0'
        expectedCorrelation.attribute2 = 'header1'
        expectedCorrelation.correlation = 1.0

        self.assertTrue(expectedCorrelation in result_dtos)

    def test_get_data_correlation_details(self):
        result_dto = analysisService.get_data_correlation(
            self.dataset, 'header0', 'header1')

        expectedCorrelationDetails = dtos.CorrelationDetailsDto()
        expectedCorrelationDetails.attribute1 = 'header0'
        expectedCorrelationDetails.data1 = [1, 3, 5]
        expectedCorrelationDetails.attribute2 = 'header1'
        expectedCorrelationDetails.data2 = [2, 4, 6]

        self.assertEqual(result_dto, expectedCorrelationDetails)
