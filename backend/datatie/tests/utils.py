
from ..services import datasetService


dataset_name = 'FIRST_DATASET'


def create_simple_dataset():
    dataset = None
    with open('datatie/tests/resources/simple_dataset.csv') as file:
        dataset = datasetService.newDataset(dataset_name, file)
    return dataset


def create_simple_dataset_with_discrete_values():
    dataset = None
    with open('datatie/tests/resources/simple_dataset_with_discrete_values.csv') as file:
        dataset = datasetService.newDataset(dataset_name, file)
    return dataset
